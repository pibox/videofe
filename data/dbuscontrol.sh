#!/bin/sh
# This is a modified version of the dbuscontrol.sh script found in the omxplayer source tree.
# It was simplified for the use cases of videofe.
# 
# Player.Action values can be found here:
# https://github.com/popcornmix/omxplayer/blob/master/KeyConfig.h
# -------------------------------------------------------------------------------------------

# Needed to or else dbus-send fails.
export DISPLAY=unix:0.0

case $1 in

# Toggle play/pause.
pause)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:16 >/dev/null
	;;

# Play is used to initialize the player.
play)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:36 >/dev/null
	;;

# Stop is the same as Exit.
stop)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:15 >/dev/null
	;;

# This jumps forward a large amount (10 seconds?)
forward)
	# dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:22 >/dev/null
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:20 >/dev/null
	;;

# Seek foward or back the specified number of microseconds.
seek)
    dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Seek int64:$2 >/dev/null
    ;;

# This jumps back a large amount (10 seconds?)
back)
	# dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:21 >/dev/null
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:19 >/dev/null
	;;

# Increment volume 
volumeup)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:18 >/dev/null
	;;

# Decrement volume 
volumedown)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:17 >/dev/null
	;;
	
# Toggles display of subtitles
togglesubtitles)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:12 >/dev/null
	;;
	
# Hide subtitles if they're visible
hidesubtitles)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:30 >/dev/null
	;;

# Show subtitles if they're hidden
showsubtitles)
	dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Action int32:31 >/dev/null
	;;

# Show video status
status)
    duration=`dbus-send --print-reply=literal --session --reply-timeout=500 --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:"org.mpris.MediaPlayer2.Player" string:"Duration"`
    [ $? -ne 0 ] && exit 1
    duration="$(echo "$duration" | awk '{print $2}')"

    position=`dbus-send --print-reply=literal --session --reply-timeout=500 --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:"org.mpris.MediaPlayer2.Player" string:"Position"`
    [ $? -ne 0 ] && exit 1
    position="$(echo "$position" | awk '{print $2}')"

    playstatus=`dbus-send --print-reply=literal --session --reply-timeout=500 --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:"org.mpris.MediaPlayer2.Player" string:"PlaybackStatus"`
    [ $? -ne 0 ] && exit 1
    playstatus="$(echo $playstatus | sed 's/^ *//;s/ *$//;')"

    paused="true"
    [ "$playstatus" == "Playing" ] && paused="false"
    echo "Duration:$duration"
    echo "Position:$position"
    echo "Paused:$paused"
    ;;

# Open a new video without stopping the player.
openuri)
    dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.OpenUri string:"$2" >/dev/null
    ;;

setposition)
    dbus-send --print-reply=literal --session --dest=org.mpris.MediaPlayer2.omxplayer /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.SetPosition objpath:/not/used int64:$2 >/dev/null
    ;;

# Usage
*)  echo "usage: $0 status|openuri|pause|stop|volumeup|volumedown|setposition [position in microseconds]|togglesubtitles|hidesubtitles|showsubtitles" >&2
exit 1
    ;;

esac
