/*******************************************************************************
 * videofe
 *
 * watcher.h:  Manage async database directory changes.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef WATCHER_H
#define WATCHER_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef WATCHER_C
extern int isWatcherProcessorRunning( void );
extern void startWatcherProcessor( void );
extern void shutdownWatcherProcessor( void );
#endif /* !WATCHER_C */
#endif /* !WATCHER_H */
