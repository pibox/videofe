/*******************************************************************************
 * videofe
 *
 * player.c:  Start and stop videos.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PLAYER_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <pibox/touchProcessor.h>

static int playerIsRunning = 0;
static pthread_mutex_t playerProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t playerProcessorThread;
static pid_t videoPid = -1;

#include "videofe.h"

/*========================================================================
 * Name:   msgPlayer
 * Prototype:  void msgPlayer( int region )
 *
 * Description:
 * Send a message to the OMX player based on the region provided.  
 *
 *   -------------------------------------------
 *   | 0: Vol Up   | 1: Next      | 2: Stop    | 
 *   -------------------------------------------
 *   | 3: Rewind   | 4:Play/Pause | 5: FF      |
 *   -------------------------------------------
 *   | 6: Vol Down | 7: Prev      | 8: Unused  |
 *   -------------------------------------------
 *
 * Arguments:
 * int region       0-8 for control, 100 is init message required at start time.
 *
 * Notes:
 * This is only valid if you're using the omxplayer with a touchscreen!
 * Return code for commands issued via system() are not checked.
 *========================================================================*/
void
msgPlayer( int region )
{
    char    *buf;
    char    left_arrow[3] = {0x1b, 0x5b, 0x43};
    char    right_arrow[3] = {0x1b, 0x5b, 0x44};

    /*
     * Ignore if not using the OMX player.
     */
    if ( !isCLIFlagSet(CLI_OMX) )
    {
        piboxLogger(LOG_ERROR, "Not using omxplayer.  Ignoring region message.\n");
        return;
    }
    piboxLogger(LOG_INFO, "omxplayer in use: region = %d\n", region);

    /*
     * Make sure dbus messaging is enabled.
     */
    if ( !isCLIFlagSet( CLI_DBUS ) )
    {
        piboxLogger(LOG_ERROR, "DBUS is not enabled.  Can't message omxplayer.\n");
        return;
    }

    piboxLogger(LOG_INFO, "Selecting action.\n");
    switch( region )
    {
        /* Volume up */
        case 0:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_VOLUP ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_VOLUP);
            piboxLogger(LOG_INFO, "Sending volume up: %s\n", buf);
            system(buf);
            free(buf);
            break;

        /* Next video */
        case 1:
            piboxLogger(LOG_INFO, "Setting up next video\n");
            if ( isCLIFlagSet(CLI_KIOSK) )
            {
                getNextVideo();
            	kioskOpenURI();
            }
            break;

        /* Stop playback */
        case 2:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_STOP ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_STOP);
            piboxLogger(LOG_INFO, "Stopping playback: %s\n", buf);
            system(buf);
            free(buf);
            if ( isCLIFlagSet(CLI_KIOSK) )
    		{
            	quitApp();
    		}
            break;

        /* Rewind */
        case 3:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_REWIND ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_REWIND);
            piboxLogger(LOG_INFO, "Rewind video: %s\n", buf);
            system(buf);
            free(buf);
            break;

        /* Play/Pause */
        case 4:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_PAUSE ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_PAUSE);
            piboxLogger(LOG_INFO, "Toggling play/pause: %s\n", buf);
            system(buf);
            free(buf);
            break;

        /* Fast forward */
        case 5:
            if ( isCLIFlagSet(CLI_KIOSK) )
            {
                /* 
                 * Only FF 10 seconds if there is at least 12 seconds left in the video.
                 * That leaves room for context switching times.
                 */
                if ( kioskTimeLeft() >= 12000000 )
                {
                    buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_SEEK ) + 11 );
                    sprintf(buf, "%s %s 10000000", DBUS_SCRIPT, DBUS_SEEK);
                    piboxLogger(LOG_INFO, "Fast forward 10 seconds in video: %s\n", buf);
                    system(buf);
                    free(buf);
                }
                else
                {
                    piboxLogger(LOG_ERROR, "Not enough time left to fast forward 10 seconds in video.\n");
                }
            }
            else
            {
                buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_FF ) + 2 );
                sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_FF);
                piboxLogger(LOG_INFO, "Fast forward in video: %s\n", buf);
                system(buf);
                free(buf);
            }
            break;

        /* Volume down */
        case 6:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_VOLDN ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_VOLDN);
            piboxLogger(LOG_INFO, "Sending volume down: %s\n", buf);
            system(buf);
            free(buf);
            break;

        /* Prev video */
        case 7:
            piboxLogger(LOG_INFO, "Setting up prev video\n");
            if ( isCLIFlagSet(CLI_KIOSK) )
            {
                getPrevVideo();
            	kioskOpenURI();
            }
            break;

        /* Unused */
        case 8:
            break;

        /* 
         * Initialization message is special
         * See http://apsvr.com/blog/?p=122
         */
        case 100:
            buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_PLAY ) + 2 );
            sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_PLAY);
            piboxLogger(LOG_INFO, "Sending initialization: %s\n", buf);
            system(buf);
            free(buf);
            break;

        /* WTF? */
        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;

    }
}

/*========================================================================
 * Name:   spawnPlayer
 * Prototype:  int spawnPlayer( char *filename )
 *
 * Description:
 * Start an video from the named file.
 *
 * Returns:
 * The process id of the video player or -1 if the player cannot be started.
 *========================================================================*/
pid_t
spawnPlayer( char *filename )
{
    struct stat stat_buf;
    pid_t       childPid;
    char        *dup, *suffix;
    char        *cmd, *str;
    char        fullpath[MAXBUF];
    char        *args[64];
    char        *arg;
    char        *ptr;
    int         filepos;
    int         found;
    int         i;

    /*
     * Arguments to launch omxplayer, which has to be run from an xterm
     * xterm -display :0.0 -fullscreen -fg black -bg black -e omxplayer -o hdmi -r <file>
     */
    char omx[MAXBUF];
    char *playerArgs = "xterm -display :0.0 -fullscreen -fg black -bg black -e ";
    char *playerArgsEmbedded[] = {
        "xterm",
        "-display", ":0.0",
        "-fullscreen",
        "-fg", "black",
        "-bg", "black",
        "-e", omx,
        (char *)NULL
        };

    /* Safety check */
    if ( filename == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing filename, can't spawn video.\n");
        return -1;
    }

    if ( stat(filename, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "No such file: %s\n", filename);
        return -1;
    }

    /* Get the filename suffix */
    dup = g_strdup( filename );
    suffix = strrchr( dup, '.' );
    if ( suffix == NULL )
    {
        piboxLogger(LOG_INFO, "Can't find suffix for file: %s\n", filename);
        return -1;
    }

    /* Find the appropiate player command */
    piboxLogger(LOG_INFO, "Looking for player for format: %s\n", suffix);
    cmd = findPlayer((char *)(suffix+1));
    g_free(dup);
    if ( cmd == NULL )
    {
        piboxLogger(LOG_INFO, "Can't find player for format: %s\n", suffix);
        return -1;
    }
    piboxLogger(LOG_INFO, "Found player for format: %s\n", cmd);

    /* Test if this is the omxplayer.  It has special support. */
    if ( strstr(cmd, "omxplayer") != NULL )
    {
        setCLIFlag(CLI_OMX);
    }

    /* Fill in omx player buffer */
    sprintf(fullpath, "'%s/%s'", cliOptions.dbTop, filename);

    /* Create command line that will run the process. */
    piboxLogger(LOG_INFO, "Starting video: %s, path = %s\n", cmd, fullpath);
    childPid = fork();
    if ( childPid == 0 )
    {   
        // Child: start player for video
        if ( isCLIFlagSet( CLI_EMBEDDED ) )
        {
            // execvp("xterm", playerArgsEmbedded);
            piboxLogger(LOG_ERROR, "Failed to spawn video player for %s: %s\n", filename, strerror(errno));
        }
        else
        {
            str = g_strdup(cmd);
            filepos = parse(str, args, 64);
            piboxLogger(LOG_INFO, "filepos = %d\n", filepos);

            /*
             * Replace %s with path to video.
             */
            found = 0;
            for(i=0; i<filepos; i++)
            {
                piboxLogger(LOG_TRACE1, "args[%d] = %s\n", i, args[i]);
                if ( strstr(args[i], "%s") != NULL )
                {
                    arg = g_malloc0(strlen(args[i])+strlen(fullpath)+2);
                    sprintf(arg, "%s", args[i]);
                    ptr = arg;
                    while (*ptr != '%' )
                        ptr++;
                    memcpy(ptr, fullpath, strlen(fullpath));
                    args[i] = arg;
                    found = 1;
                    break;
                }
            }

            /* Rebuild the command to run as a single string */
            memset(omx, 0, MAXBUF);
            for(i=0; args[i]!=NULL; i++)
            {
                strcat(omx, args[i]);
                strcat(omx, " ");
            }

            /*
             * If this is the omx player, use playerArgs.
             * parse the playerArgs and replace the trailing NULL with the updated command.
             */
            memset(args, 0, sizeof(args));
            if ( isCLIFlagSet(CLI_OMX) )
            {
                str = g_strdup(playerArgs);
                filepos = parse(str, args, 64);
                args[--filepos] = omx;
                args[++filepos] = NULL;
            }
            else
            {
                /* 
                 * Just parse the command - it's not omxplayer and doesn't need xterm to run it.
                 */
                str = g_strdup(omx);
                filepos = parse(str, args, 64);
            }

            /* Dump the args we exec */
            for(i=0; args[i]!=NULL; i++)
            {
                piboxLogger(LOG_INFO, "Argument[%d]: %s\n", i, args[i]);
            }

            if ( found )
            {
                execvp(args[0], args);
                piboxLogger(LOG_ERROR, "Failed to spawn video player cmd %s, path = %s: %s\n", 
                        cmd, fullpath, strerror(errno));
            }
            else
            {
                piboxLogger(LOG_ERROR, "Missing \\%s argument in player cmd: %s\n", cmd);
            }
        }
        abort();
    }

    /* Wait a couple of seconds and then send an init message to the player */
    if ( isCLIFlagSet(CLI_OMX) && isCLIFlagSet(CLI_TOUCH) )
    {
        sleep(2);
        msgPlayer(100);
    }

    return childPid;
}

/*========================================================================
 * Name:   killPlayer
 * Prototype:  int killPlayer( pid_t pid)
 *
 * Description:
 * Kill a video player based on the process ID and reap the child process.
 *========================================================================*/
void
killPlayer( pid_t pid )
{
    int status;
    kill(pid, SIGTERM);
    pid = waitpid( pid, &status, 0);
    unsetCLIFlag(CLI_OMX);
}


/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isPlayerProcessorRunning
 * Prototype:  int isPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of playerIsRunning variable.
 *========================================================================*/
int
isPlayerProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &playerProcessorMutex );
    status = playerIsRunning;
    pthread_mutex_unlock( &playerProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setPlayerProcessorRunning
 * Prototype:  int setPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of playerIsRunning variable.
 *========================================================================*/
static void
setPlayerProcessorRunning( int val )
{
    pthread_mutex_lock( &playerProcessorMutex );
    playerIsRunning = val;
    pthread_mutex_unlock( &playerProcessorMutex );
}

/*========================================================================
 * Name:   playerProcessor
 * Prototype:  void playerProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from player and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This thread starts once and waits for the player to exit.
 *========================================================================*/
static void *
playerProcessor( void *arg )
{
    int         status = -1;
    char        cmd[MAXBUF];

    /* Register a callback for touch events. */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering msgPlayer.\n");
        piboxTouchRegisterCB(msgPlayer, TOUCH_REGION);
    }

    /* Spawn the video player */
    videoPid = spawnPlayer((char *)arg);
    if ( videoPid == -1 )
        return(0);
    setPlayerProcessorRunning(1);

    /* Wait to reap child */
    videoPid = waitpid( videoPid, &status, 0);
    videoPid = -1;

    /* Reregister videofe handler, if necessary */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering videoTouch.\n");
        piboxTouchRegisterCB(videoTouch, TOUCH_REGION);
    }

    /*
     * Now tell the X.org display to refresh 'cuz omxplayer trashed the framebuffer.
     * Changing VTs was the only way to guarantee the display was updated with
     * X updates.
     */
    sprintf(cmd, "openvt -s -w -- sh -c \"sleep 1\"");
    piboxLogger(LOG_INFO, "VT switch command: %s\n",cmd);
    system(cmd);
    usleep(10000);
    system("xrefresh -display :0.0");

    setPlayerProcessorRunning(0);

    piboxLogger(LOG_INFO, "Player thread is exiting.\n");
    return(0);
}

/*========================================================================
 * Name:   startPlayerProcessor
 * Prototype:  void startPlayerProcessor( char *filename )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownPlayerProcessor().
 *========================================================================*/
void
startPlayerProcessor( char *filename )
{
    int rc;

    /* Prevent running two at once. */
    if ( isPlayerProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&playerProcessorThread, NULL, &playerProcessor, (void *)filename);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create player thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started player thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownPlayerProcessor
 * Prototype:  void shutdownPlayerProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownPlayerProcessor( void )
{
    int timeOut = 0;
    int isRunning = -1;

    // Try to kill any running video.
    if ( videoPid != -1 )
        killPlayer(videoPid);

    if ( isPlayerProcessorRunning() )
    {
        while ( isPlayerProcessorRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 60)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on player thread to shut down.\n");
                return;
            }
        }
        pthread_detach(playerProcessorThread);
    }

    piboxLogger(LOG_INFO, "playerProcessor shut down.\n");
}

