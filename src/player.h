/*******************************************************************************
 * videofe
 *
 * player.h:  Start and stop videos.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H

/*========================================================================
 * Defined values
 *=======================================================================*/
#define OMX_FIFO         "/tmp/omx.f"
#define DBUS_SCRIPT      "/usr/bin/dbuscontrol.sh"
#define DBUS_VOLUP       "volumeup"
#define DBUS_VOLDN       "volumedown"
#define DBUS_PAUSE       "pause"
#define DBUS_PLAY        "play"
#define DBUS_STOP        "stop"
#define DBUS_SEEK        "seek"
#define DBUS_FF          "forward"
#define DBUS_REWIND      "back"
#define DBUS_STATUS      "status"
#define DBUS_OPENURI     "openuri"
#define DBUS_SUBTITLES   "togglesubtitles"
#define DBUS_SETPOSITION "setposition"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PLAYER_C
extern int isPlayerProcessorRunning( void );
extern void startPlayerProcessor( char * );
extern void shutdownPlayerProcessor( void );
#endif /* !PLAYER_C */
#endif /* !PLAYER_H */
