/*******************************************************************************
 * videofe
 *
 * dbkiosk.h:  Functions for playing videos in kiosk mode.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DBKIOSK_H
#define DBKIOSK_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DBKIOSK_C
extern char *kioskActiveVideo;
extern void dbLoadKiosk( void );
extern void getNextVideo( void );
extern void getPrevVideo( void );
extern void kioskStartStatusTimer( void );
extern void kioskStartNextTimer( void );
extern void kioskOpenURI( void );
extern int  kioskTimeLeft( void );
#else
/* Prototypes for dbkiosk.c */
void getNextVideo( void );
void kioskOpenURI();
void kioskPause();
void kioskPlay();
#endif

#endif /* DBKIOSK_H */
