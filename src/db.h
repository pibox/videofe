/*******************************************************************************
 * videofe
 *
 * db.h:  Functions for reading a VideoLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

#define DB_VALID    1
#define DB_INVALID  2

/*
 * A movie entry
 */
typedef struct _movie_t {
    char    *basedir;
    char    *json;
    char    *title;
    char    *path;
    char    *poster;
    char    *overview;
    char    valid;
} MOVIE_T;

/*
 * A TV Episodes entry
 */
typedef struct _episode_t {
    char    *basedir;
    char    *series_json;
    char    *episode_id;
    char    *series_id;
    char    *episode_title;
    char    *real_title;
    char    *eposter;
    char    *sposter;
    char    *overview;
    char    *path;
    char    valid;
} EPISODE_T;

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

// Production locations for local media on USB sticks
#define DBTOP       "/media"

// Test location for local media
#define DBTOP_T     "data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern int  watchDirs ( void );
extern void stopWatchDirs ( void );
extern int  getWatchFD ( void );
extern void dbLoad( void );
extern void populateList (GtkListStore *listStore);
extern void updatePoster (GtkTreeSelection *, gpointer);
extern int  playVideo( void );
#endif

#endif /* DB_H */
