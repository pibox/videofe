/*******************************************************************************
 * videofe
 *
 * video.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef VIDEOFE_H
#define VIDEOFE_H

#include <gtk/gtk.h>

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "videofe"
#define MAXBUF      4096

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/* DB types based on user selection */
#define DB_MOVIE        0
#define DB_TV           1

/* Where the config file is located */
#define F_CFG           "/etc/videofe.cfg"
#define F_CFG_T         "data/videofe.cfg"
#define F_DISPLAYCFG_T  "data/pibox-config"
#define F_DISPLAYCFG    "/etc/pibox-config"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef VIDEOFE_C
extern void 	 updateDisplay( void );
extern void      videoTouch( int region );
extern void 	 quitApp( void );
extern void      do_drawing();
extern guint     getChoice( void );
extern gboolean  startKiosk (gpointer data);
extern gboolean  updateOnce (gpointer data);
#endif

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef VIDEOFE_C
int     daemonEnabled = 0;
char    errBuf[MAXBUF];
gchar   *imagePath = NULL;
gchar   *seriesImagePath = NULL;
gchar   *overview = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
extern gchar   *seriesImagePath;
extern gchar   *overview;
#endif /* VIDEOFE_C */

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "player.h"
#include "db.h"
#include "dbkiosk.h"
#include "watcher.h"
#include "cli.h"
#include "utils.h"

#endif /* !VIDEOFE_H */

