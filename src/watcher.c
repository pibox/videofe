/*******************************************************************************
 * videofe
 *
 * watcher.c:  Manage async database directory changes.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define WATCHER_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

static int watcherIsRunning = 0;
static pthread_mutex_t watcherProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t watcherProcessorThread;

#include "videofe.h"

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isWatcherProcessorRunning
 * Prototype:  int isWatcherProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of watcherIsRunning variable.
 *========================================================================*/
int
isWatcherProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &watcherProcessorMutex );
    status = watcherIsRunning;
    pthread_mutex_unlock( &watcherProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setWatcherProcessorRunning
 * Prototype:  int setWatcherProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of watcherIsRunning variable.
 *========================================================================*/
static void
setWatcherProcessorRunning( int val )
{
    pthread_mutex_lock( &watcherProcessorMutex );
    watcherIsRunning = val;
    pthread_mutex_unlock( &watcherProcessorMutex );
}

/*========================================================================
 * Name:   watcherProcessor
 * Prototype:  void watcherProcessor( CLI_T * )
 *
 * Description:
 * Wait on changes to inotify file descriptor.  When they happen,
 * update the database as appropriate.
 *
 * Input Arguments:
 * void *arg    Unused, but required by API.
 *
 * Notes:
 * This thread starts once and waits for the watcher to exit.
 *========================================================================*/
static void *
watcherProcessor( void *arg )
{
    int             watcherfd = -1;
    fd_set          rfds;
    struct timeval  tv;
    int             retval;
    char            buf[4096];

    /* Mark the processor loop as running. */
    setWatcherProcessorRunning(1);

    /* Setup inotify from db.c */
    watcherfd = watchDirs();
    if ( watcherfd == -1 )
    {
        piboxLogger(LOG_ERROR, "Can't watch db directories.\n");
        goto watcherExit;
    }

    /* Set loop to continue running. */
    setCLIFlag(CLI_WATCHER);

    /* Run the queue periodically to expire abandoned streams. */
    while( isCLIFlagSet(CLI_WATCHER) )
    {   
        /* Watch the timer fd for activity. */
        FD_ZERO(&rfds);
        FD_SET(watcherfd, &rfds);

        /*
         * Reset select()'s timeout on each run.
         */
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        retval = select(watcherfd+1, &rfds, NULL, NULL, &tv);

        /* A select timeout with no fds or an error just makes us wait again, unless CLI_WATCHER has been disabled. */
        if ( retval == 0 )
        {   
            piboxLogger(LOG_TRACE4, "select: timeout\n");
            continue;
        }
        if ( retval < 0 )
        {   
            piboxLogger(LOG_TRACE1, "error on select(): %s\n", strerror(errno));
            continue;
        }

        /* Read the event - but we don't care about what's in it. */
        read(watcherfd, buf, sizeof buf);

        /* Wait a moment for the filesystems to be mounted and fully available. */
        piboxLogger(LOG_INFO, "DB change detected.\n");
        sleep(5);

        /* We've been inotified - rebuild the dbs. */
        dbLoad();

        /* Must run display updates in the GTK+ main thread. */
        g_timeout_add(10, updateOnce, NULL);
    }

watcherExit:
    piboxLogger(LOG_INFO, "Watcher thread is exiting.\n");
    unsetCLIFlag(CLI_WATCHER);
    setWatcherProcessorRunning(0);
    return(0);
}

/*========================================================================
 * Name:   startWatcherProcessor
 * Prototype:  void startWatcherProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownWatcherProcessor().
 *========================================================================*/
void
startWatcherProcessor( void )
{
    int rc;

    /* Prevent running two at once. */
    if ( isWatcherProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&watcherProcessorThread, NULL, &watcherProcessor, NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create watcher thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started watcher thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownWatcherProcessor
 * Prototype:  void shutdownWatcherProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownWatcherProcessor( void )
{
    int timeOut = 0;

    /* Disable watcher loop */
    unsetCLIFlag(CLI_WATCHER);

    /* Wait for thread to exit. */
    if ( isWatcherProcessorRunning() )
    {
        while ( isWatcherProcessorRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 60)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on watcher thread to shut down.\n");
                return;
            }
        }
        pthread_detach(watcherProcessorThread);
    }

    piboxLogger(LOG_INFO, "watcherProcessor shut down.\n");
}

