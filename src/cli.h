/*******************************************************************************
 * PiBox service daemon
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_EMBEDDED        0x00001    // Run in embedded mode (player runs from xterm)
#define CLI_DBUS            0x00002    // Enable dbus messaging
#define CLI_WATCHER         0x00004    // Watcher thread is enabled if this is set.
#define CLI_LOGTOFILE       0x00008    // Enable log to file
#define CLI_TEST            0x00020    // Enable test mode (read data files locally)
#define CLI_ROOT            0x00040    // Running as root
#define CLI_TOUCH           0x00200    // Using a touch screen
#define CLI_OMX             0x00400    // Using the omxplayer for playback
#define CLI_SMALL_SCREEN    0x00800    // If set, we're on a small screen. 
#define CLI_KIOSK           0x01000    // Kiosk Mode (play videos in sequence)
#define CLI_KIOSK_PENDING   0x02000    // Kiosk Mode (play videos in sequence)
#define CLI_KIOSK_EXIT      0x04000    // Exit Kiosk Mode 

typedef struct _cli_t {
    int     flags;      // Enable/disable features
    int     verbose;    // Sets the verbosity level for the application
    char    *logFile;   // Name of local file to write log to
    int     vt;         // vt the X display is on
    int     vttmp;      // vt to jump to when running omxplayer
    GSList  *player;    // video player commands
    char    *dbTop;     // Top of the tree to search for db's
    char    *display_type;          // What type of display is in use?
    char    *display_resolution;    // What is the display resolution.
} CLI_T;

typedef struct _player_t {
    char    *formats;   // File formats, based on file suffix, supported by this player.
    char    *cmd;       // Command to run for this file type.
} PLAYER_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/
#define DEFAULT_PLAYER  "omxplayer -p --font /usr/share/fonts/pibox -r %s"

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "ekTd:l:v:"
#define USAGE \
"\n\
videofe [ -ekT | -d <dir> | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -T              Use test files (for debugging only) \n\
    -k              Enable kiosk mode (play videos in sequence) \n\
    -e              Enable embedded mode (run player from xterm) \n\
    -d dir          Top directory for video database \n\
    -l filename     Enable local logging to named file \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
void initConfig( void );
void validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
char *findPlayer( char *format );
#else
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern void validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
extern char *findPlayer( char *format );
#endif

#endif /* !CLI_H */
