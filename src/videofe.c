/*******************************************************************************
 * videofe
 *
 * videofe.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define VIDEOFE_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>

#include "videofe.h"

/* Local prototypes */
void do_menu_drawing();
static gboolean key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data);
void videoTouchGTK( int region );

GtkWidget       *window;
GtkWidget       *menuArea;
GtkWidget       *darea;
GtkWidget       *blankarea;
GtkWidget       *view;
GtkListStore    *videoStore;
cairo_surface_t *image = NULL;
cairo_t         *cr = NULL;
cairo_t         *menucr = NULL;

// The home key is the key that exits the app.
guint homeKey = -1;

/*
 * DB Choice
 * 0: Movies
 * 1: TV
 */
guint db_choice = 0;

/* 
 * Font sizes depend on screen size.
 * Defaults to a normal sized screen.
 * Small screens reduce this size.
 */
gint largeFont  = 25;
gint mediumFont = 15;
gint smallFont  = 13;
gint summaryFont= 20;
gint listFont   = 20;

/*
 *========================================================================
 * Name:   updateDisplay
 * Prototype:  void updateDisplay( void )
 *
 * Description:
 * Update the displayed list of videos based on latest DB.
 *========================================================================
 */
void
updateDisplay( void )
{
    GtkTreePath         *tree_path = NULL;
    GtkTreeSelection    *tree_selection = NULL;

    piboxLogger(LOG_TRACE1, "Calling populateList\n");
    populateList(videoStore);
    tree_path = gtk_tree_path_new_from_indices(0,-1);
    tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
    gtk_tree_selection_select_path(tree_selection, tree_path);
    gtk_tree_path_free(tree_path);
    updatePoster(tree_selection, darea);
}

/*
 *========================================================================
 * Name:   videoTouch
 * Prototype:  void videoTouch( int region )
 *
 * Description:
 * Handler for touch region reports.  Basically it's a front end to the
 * key and button press handlers.
 *
 * In Movies/TV Mode:
 *   -----------------------------------
 *   | 0: N/A | 1: Up list   | 2: Quit | 
 *   -----------------------------------
 *   | 3: Tab | 4: Select    | 5: Tab  |
 *   -----------------------------------
 *   | 6: N/A | 7: Down list | 8: N/A  |
 *   -----------------------------------
 *========================================================================
 */
void
videoTouch( int region )
{
    g_idle_add( (GSourceFunc)videoTouchGTK, GINT_TO_POINTER(region) );
}

void
videoTouchGTK( int region )
{
    static int          idx = 0;
    static int          inprogress = 0;
    GtkTreePath         *tree_path = NULL;
    GtkTreeSelection    *tree_selection = NULL;
    GdkEvent            *event;

    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return;
    }
    inprogress = 1;

    switch( region )
    {
        case 0:
            piboxLogger(LOG_INFO, "Ctrl-Q key\n");
            gtk_main_quit();
            break;

        /* Up list */
        case 1:
            piboxLogger(LOG_INFO, "Up list \n");
            idx--;
            if (idx<0)
                idx=0;
            tree_path = gtk_tree_path_new_from_indices(idx,-1);
            tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
            gtk_tree_selection_select_path(tree_selection, tree_path);
            gtk_tree_path_free(tree_path);
            break;

        case 2:
            piboxLogger(LOG_INFO, "Ctrl-Q key\n");
            gtk_main_quit();
            break;

        /* Tab */
        case 3:
            piboxLogger(LOG_INFO, "Tab 3\n");
            db_choice = !db_choice;
            do_menu_drawing();
            updateDisplay();
            idx=0;
            break;

        /* Select */
        case 4:
            piboxLogger(LOG_INFO, "Select\n");
            if ( playVideo() == FALSE )
            {
                db_choice = !db_choice;
                do_menu_drawing();
                updateDisplay();
            }
            break;

        /* Tab */
        case 5:
            piboxLogger(LOG_INFO, "Tab 5\n");
            db_choice = !db_choice;
            do_menu_drawing();
            updateDisplay();
            idx=0;
            break;

        case 6:
            break;

        /* Down list */
        case 7:
            piboxLogger(LOG_INFO, "Down list\n");
            idx++;
            /*
             * This needs db.c:getListSize(db_choice) to know max value. 
            if (idx>xx)
                idx=xx;
                */
            tree_path = gtk_tree_path_new_from_indices(idx,-1);
            tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
            gtk_tree_selection_select_path(tree_selection, tree_path);
            gtk_tree_path_free(tree_path);
            break;

        case 8:
            break;

        /* WTF? */
        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;
    }
    inprogress = 0;
}

/*
 *========================================================================
 * Name:   quitApp
 * Prototype:  void quitApp( void )
 *
 * Description:
 * Quit the app via gtk_main_quit.  This allows external modules to exit
 * without forcing them to have gtk knowledge.
 *========================================================================
 */
void
quitApp( void )
{
    gtk_main_quit();
}

/*
 *========================================================================
 * Name:   getChoice
 * Prototype:  guint getChoice( void )
 *
 * Description:
 * Retrieves the current DB choice: DB_MOVIE or DB_TV
 *
 * Returns:
 * DB_MOVIE if user is browsing movies
 * DB_TV if user is browsing TV Episodes
 *========================================================================
 */
guint
getChoice( void )
{
    return (db_choice);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig;
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
        largeFont  = 18;
        mediumFont = 12;
        smallFont  = 8;
        summaryFont= 15;
        listFont   = 10;
    }

    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}
/*
 *========================================================================
 * Name:   do_blank_drawing
 * Prototype:  void do_blank_drawing( void )
 *
 * Description:
 * Draws a black background for kiosk mode.
 *========================================================================
 */
void
do_blank_drawing()
{
    gfloat                  width, height;
    GtkRequisition          req;

    /*
     * Setup Cairo
     */
    piboxLogger(LOG_INFO, "Getting cr\n");
    cr = gdk_cairo_create(darea->window);
    req.width = gdk_window_get_width(darea->window);
    req.height = gdk_window_get_height(darea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    /* 
     * Black background
     */
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 1.0);
    cairo_rectangle(cr, 0, req.height, req.width, req.height);
    cairo_fill(cr);

    cairo_destroy(cr);
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( void )
 *
 * Description:
 * Updates the poster area of the display.
 *========================================================================
 */
void
do_drawing()
{
    char                    buf[32];
    struct stat             stat_buf;
    gfloat                  width, height;
    gint                    offset_x, offset_y;
    gfloat                  x_scaling;
    gfloat                  y_scaling;
    GtkRequisition          req;
    cairo_text_extents_t    extents;
    cairo_t                 *ic;
    PangoLayout             *layout;
    PangoFontDescription    *desc;

    piboxLogger(LOG_INFO, "Entered\n");
    if ( cr != NULL )
        cairo_destroy(cr);
    if ( image != NULL )
    {
        cairo_surface_destroy(image);
        image = NULL;
    }

    piboxLogger(LOG_INFO, "Getting cr\n");
    cr = gdk_cairo_create(darea->window);
    req.width = gdk_window_get_width(darea->window);
    req.height = gdk_window_get_height(darea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // If poster doesn't exist, draw some text instead.
    if ( (imagePath == NULL) || (stat(imagePath, &stat_buf) != 0) )
    {
        // Fill with white background
        cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
        cairo_set_line_width(cr, 1);
        cairo_rectangle(cr, 0, 0, req.width, req.height);
        cairo_stroke_preserve(cr);
        cairo_fill(cr);

        piboxLogger(LOG_INFO, "imagePath does not exist, skipping poster\n");
        cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
        cairo_select_font_face(cr, "Courier",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);

        piboxLogger(LOG_INFO, "largeFont: %d\n", largeFont);
        cairo_set_font_size(cr, largeFont);
        cairo_text_extents(cr, "No poster available", &extents);
        cairo_move_to(cr, req.width/2 - extents.width/2, req.height/2);
        cairo_show_text(cr, "No poster available");
        return;
    }

    if ( seriesImagePath != NULL )
    {
        image = cairo_image_surface_create_from_png(seriesImagePath);
        width = cairo_image_surface_get_width(image);
        height = cairo_image_surface_get_height(image);
        piboxLogger(LOG_INFO, "series image w/h: %f / %f\n", width, height);

        // Scaling to fit
        x_scaling = req.width / width;
        y_scaling = (req.height/2) / height;
        piboxLogger(LOG_INFO, "x/y scaling: %f / %f\n", x_scaling, y_scaling);
        cairo_scale (cr, x_scaling, y_scaling);
        cairo_set_source_surface (cr, image, 0, 0);

        cairo_paint(cr);

        /* Make a new cairo surface to work on for the episode poster */
        cairo_destroy(cr);
        cr = gdk_cairo_create(darea->window);
    }

    /* Paint the movie or episode poster */
    image = cairo_image_surface_create_from_png(imagePath);
    width = cairo_image_surface_get_width(image);
    height = cairo_image_surface_get_height(image);
    piboxLogger(LOG_INFO, "image w/h: %f / %f\n", width, height);

    // Scaling to fit
    if ( seriesImagePath != NULL )
    {
        cairo_translate(cr, 0, req.height/2);
        y_scaling = (req.height/2) / height;
    }
    else
    {
        y_scaling = req.height / height;
    }
    x_scaling = req.width / width;
    piboxLogger(LOG_INFO, "x/y scaling: %f / %f\n", x_scaling, y_scaling);
    cairo_scale (cr, x_scaling, y_scaling);
    cairo_set_source_surface (cr, image, 0, 0);

    cairo_paint(cr);

    /* 
     * Add a semi-transparent block over the image.
     * This will hold the overview.
     */
    cairo_destroy(cr);
    cr = gdk_cairo_create(darea->window);
    cairo_set_source_rgba(cr, 0.25, 0.25, 0.25, 0.75);
    cairo_rectangle(cr, 0, req.height/2, req.width, req.height/2);
    cairo_fill(cr);

    /*
     * Move to the upper left, with a slight indent.
     */
    cairo_destroy(cr);
    cr = gdk_cairo_create(darea->window);
    cairo_translate(cr, 10, req.height/2+10);
    layout = pango_cairo_create_layout(cr);

    /*
     * Generate text that is wrapped in the gray box.
     */
    sprintf(buf, "Sans Bold %d", summaryFont);
    desc = pango_font_description_from_string(buf);
    // desc = pango_font_description_from_string("Sans Bold 20");
    // pango_font_description_set_absolute_size(desc, 20*PANGO_SCALE);
    pango_font_description_set_absolute_size(desc, summaryFont*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);

    pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
    pango_layout_set_width(layout, ((int)(req.width)-20)*PANGO_SCALE);

    pango_layout_set_text(layout, overview, -1);

    cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
    pango_cairo_update_layout(cr, layout);
    pango_cairo_show_layout(cr, layout);

    g_object_unref(layout);
}

/*
 *========================================================================
 * Name:   do_menu_drawing
 * Prototype:  void do_menu_drawing( void )
 *
 * Description:
 * Updates the menu area of the display.
 *
 * Notes:
 * See http://zetcode.com/gfx/cairo/cairotext/
 *========================================================================
 */
void
do_menu_drawing()
{
    GtkRequisition req;

    menucr = gdk_cairo_create(menuArea->window);
    req.width = gdk_window_get_width(menuArea->window);
    req.height = gdk_window_get_height(menuArea->window);
    piboxLogger(LOG_INFO, "window w/h: %d / %d\n", req.width, req.height);

    // Fill with black background
    cairo_set_source_rgb(menucr, 0.0, 0.0, 0.0);
    cairo_set_line_width(menucr, 1);
    cairo_rectangle(menucr, 0, 0, req.width, req.height);
    cairo_stroke_preserve(menucr);
    cairo_fill(menucr);

    cairo_select_font_face(menucr, "Purisa",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    if ( !db_choice )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 20, 25);
    cairo_show_text(menucr, "Movies");

    if ( db_choice )
    {
        cairo_set_font_size(menucr, mediumFont);
        cairo_set_source_rgb(menucr, 1.0, 1.0, 1.0);
    }
    else
    {
        cairo_set_font_size(menucr, smallFont);
        cairo_set_source_rgb(menucr, 0.75, 0.75, 0.75);
    }
    cairo_move_to(menucr, 220, 25);
    cairo_show_text(menucr, "TV Episodes");
}

/*
 *========================================================================
 * Name:   on_draw_event
 * Prototype:  void on_draw_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean
on_draw_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    if ( isCLIFlagSet( CLI_KIOSK ) )
    {
        do_blank_drawing();
    }
    else
    {
        do_drawing();
    }
}

/*
 *========================================================================
 * Name:   on_menudraw_event
 * Prototype:  void on_menudraw_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the menu area of the display.
 *========================================================================
 */
static gboolean
on_menudraw_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    do_menu_drawing();
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start a video.
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event)
{
    switch(event->type) {
        case GDK_2BUTTON_PRESS:
            // g_print("Mouse button %d double-click at coordinates (%lf,%lf)\n", event->button,event->x,event->y);
            if ( playVideo() == FALSE )
            {
                db_choice = !db_choice;
                do_menu_drawing();
                updateDisplay();
            }
            break;
        default:
            break;
    }
    return(TRUE);
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    GtkTreePath         *tree_path;
    GtkTreeSelection    *tree_selection;

    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        case GDK_KEY_Tab:
            piboxLogger(LOG_INFO, "Tab key\n");
            db_choice = !db_choice;
            do_menu_drawing();
            updateDisplay();
            return(TRUE);
            break;

        case GDK_KEY_space:
        case GDK_KEY_Return:
        case GDK_KEY_KP_Space:
        case GDK_KEY_KP_Enter:
            piboxLogger(LOG_INFO, "Select key\n");
            if ( playVideo() == FALSE )
            {
                db_choice = !db_choice;
                do_menu_drawing();
                updateDisplay();
            }
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   createKioskWindow
 * Prototype:  GtkWidget *createKioskWindow( void )
 *
 * Description:
 * Creates a black top level window.  This is needed in kiosk mode to avoid
 * the flashing that comes with changing videos.
 *========================================================================
 */
GtkWidget *
createKioskWindow()
{
    GtkWidget           *vbox;
    GtkWidget           *hbox1;
    GtkTreeViewColumn   *col;
    GtkCellRenderer     *renderer;
    GtkTreeSelection    *select;
    GtkWidget           *sw;
    char                buf[32];

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    /*
     * The blank area
     */
    darea = gtk_drawing_area_new();
    gtk_widget_set_size_request( darea, piboxGetDisplayWidth(), piboxGetDisplayHeight());
    g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_draw_event), NULL);
    gtk_container_add (GTK_CONTAINER (window), darea);

    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), piboxGetDisplayWidth(), piboxGetDisplayHeight());
    gtk_window_set_title(GTK_WINDOW(window), "VideoFE Kiosk");

    return window;
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *vbox;
    GtkWidget           *hbox1;
    GtkTreeViewColumn   *col;
    GtkCellRenderer     *renderer;
    GtkTreeSelection    *select;
    GtkWidget           *sw;
    char                buf[32];

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // Two rows: menu and content
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /* Menu area */
    menuArea = gtk_drawing_area_new();
    gtk_widget_set_size_request( menuArea, 800, 40);
    gtk_box_pack_start (GTK_BOX (vbox), menuArea, FALSE, FALSE, 0);

    gtk_widget_add_events(menuArea, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(menuArea), "expose_event", G_CALLBACK(on_menudraw_event), NULL);

    // Two columns: list and poster
    hbox1 = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox1, "hbox1");
    gtk_widget_show (hbox1);
    gtk_box_pack_start (GTK_BOX (vbox), hbox1, TRUE, TRUE, 0);

    // Populate a video list model
    videoStore = gtk_list_store_new (1, G_TYPE_STRING);
    populateList(videoStore);

    /*
     * Create a scrolled window for the tree view
     */
    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_widget_show (sw);
    gtk_box_pack_start (GTK_BOX (hbox1), sw, TRUE, TRUE, 0);

    /*
     * Create the view of the video list
     */
    view = gtk_tree_view_new();
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(view), FALSE);
    renderer = gtk_cell_renderer_text_new();
    // g_object_set(renderer, "weight", PANGO_WEIGHT_BOLD, NULL);
    sprintf(buf, "Sans Normal %d", listFont);
    g_object_set(renderer, "font", buf, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                                 -1,
                                                 NULL,
                                                 renderer,
                                                 "text", 0,
                                                 NULL);

    gtk_tree_view_set_model( GTK_TREE_VIEW(view), GTK_TREE_MODEL(videoStore) );
    g_object_unref(G_OBJECT(videoStore));
    gtk_widget_show (view);
    gtk_container_add (GTK_CONTAINER (sw), view);

    /*
     * The poster area
     */
    darea = gtk_drawing_area_new();
    gtk_widget_set_size_request( darea, 600, 800);
    g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_draw_event), NULL);
    gtk_box_pack_start (GTK_BOX (hbox1), darea, FALSE, FALSE, 0);

    // And add click events to the drawing area.
    gtk_widget_add_events(darea, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(darea), "button_press_event", G_CALLBACK(button_press), NULL);

    /*
     * Setup for selecting from the view.
     */
    select = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
    gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
    g_signal_connect (G_OBJECT (select), "changed",
                      G_CALLBACK (updatePoster),
                      darea);

    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 2048, 1024);
    gtk_window_set_title(GTK_WINDOW(window), "VideoFE");

    return window;
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            // TBD
            break;

        case SIGINT:
            piboxTouchSignalHandler();
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;
    }
}

/*
 *========================================================================
 * Name:   updateOnce
 * Prototype:  gboolean updateOnce( gpointer )
 *
 * Description:
 * Called once to update the display, typically from an inotify event.
 *========================================================================
 */
gboolean updateOnce (gpointer data)
{
    /* Update the display, assuming we've refreshed our lists. */
    updateDisplay();

    /* Don't run this again. */
    return FALSE;
}

/*
 *========================================================================
 * Name:   startKiosk
 * Prototype:  gboolean startKiosk( gpointer )
 *
 * Description:
 * Called once to start video playback.
 *========================================================================
 */
gboolean startKiosk (gpointer data)
{
    int count=0;

    startPlayerProcessor(kioskActiveVideo);
    while ( !isPlayerProcessorRunning && count<10 )
    {
        count++;
        sleep(1);
    }
    if ( !isPlayerProcessorRunning )
    {
        piboxLogger(LOG_ERROR, "Failed to start player.\n");
    }
    else
    {
        /* Start the status and next timers, but offset by 10ms to help avoid locking issues. */
        kioskStartStatusTimer();
        usleep(10000);
        kioskStartNextTimer();
    }

    /* Don't run this again. */
    return FALSE;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    cwd[512];
    char    gtkrc[1024];

    GtkWidget *window;
    struct stat stat_buf;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    // piboxLoggerSetFlags(LOG_F_FIXEDHDR);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());
    piboxLogger(LOG_INFO, "Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    piboxLogger(LOG_INFO, "vtsrc: %d\n", cliOptions.vt);
    piboxLogger(LOG_INFO, "vttmp: %d\n", cliOptions.vttmp);

    // Read environment config for keyboard behaviour
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();
    piboxLogger(LOG_INFO, "display type: %s\n", cliOptions.display_type);
    piboxLogger(LOG_INFO, "display resolution: %s\n", cliOptions.display_resolution);

    // Set the path to the top of the video libraries.
    if ( cliOptions.dbTop == NULL )
    {
        piboxLogger(LOG_INFO, "dbtop is null\n");
        if ( isCLIFlagSet( CLI_TEST) )
            cliOptions.dbTop = DBTOP_T;
        else
            cliOptions.dbTop = DBTOP;
    }
    else
    {
        piboxLogger(LOG_INFO, "dbtop is not null\n");
    }

    // Test for db existance.
    if ( stat(cliOptions.dbTop, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory");
        return(-1);
    }
    piboxLogger(LOG_INFO, "Reading database directory: %s\n", cliOptions.dbTop);

    // Change to that directory
    memset(cwd, 0, 512);
    getcwd(cwd, 512);
    sprintf(gtkrc, "%s/data/gtkrc", cwd);
    piboxLogger(LOG_INFO, "gtkrc: %s\n", gtkrc);
    chdir(cliOptions.dbTop);

    piboxLogger(LOG_INFO, "Running from: %s\n", cliOptions.dbTop);
 
    /* 
     * If we're on a touchscreen
     * a. launch the input handler 
     * b. Register a handler for us
     * Enable dbus messaging if we can find the messaging script, which we should have installed 
     * with videofe!
     */
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        if ( stat(DBUS_SCRIPT, &stat_buf) == 0 )
        {
            /* Enable dbus messaging */
            setCLIFlag( CLI_DBUS );
        }
        else
        {
            /* Keep dbus messaging disabled. */
            piboxLogger(LOG_ERROR, "Dbus messaging is disabled - Can't find dbus script: %s\n", DBUS_SCRIPT);
        }

        piboxLogger(LOG_INFO, "Registering videoTouch.\n");
        piboxTouchRegisterCB(videoTouch, TOUCH_REGION);
        piboxTouchStartProcessor();
    }

    gtk_init(&argc, &argv);

    // Load db
    if ( isCLIFlagSet( CLI_KIOSK ) )
    {
        piboxLogger(LOG_INFO, "Kiosk mode is enabled.\n");
        window = createKioskWindow();
        gtk_window_fullscreen (GTK_WINDOW(window));
        gtk_widget_show_all(window);

        dbLoadKiosk();
        g_timeout_add(1000, startKiosk, NULL);
    }
    else
    {
        dbLoad();

        /* Start the watcher that checks for db changes on disk. */
        startWatcherProcessor();

        if ( isCLIFlagSet( CLI_TEST) )
            gtk_rc_parse(gtkrc);
        else
            piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
        window = createWindow();
        gtk_window_fullscreen (GTK_WINDOW(window));
        gtk_widget_show_all(window);

        // Set keyboard focus on the list of movies
        gtk_widget_grab_default(view);
        gtk_widget_grab_focus(view);
    }

    gtk_main();
    shutdownWatcherProcessor();
    shutdownPlayerProcessor();
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        /* Touch processor blocks in a read, so we need to SIGINT to break out of it. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }
    piboxLoggerShutdown();
    return 0;
}
