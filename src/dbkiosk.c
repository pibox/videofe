/*******************************************************************************
 * videofe
 *
 * dbkiosk.c:  Functions for managing video files in kiosk mode.
 *
 * Kiosk mode just plays videos without user interaction.  It plays them 
 * sorted order by filename.  All files found under the current directory
 * that match a set of supported (and common) video formats can be played.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DBKIOSK_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "videofe.h"

#define STALL_MAX       10

/*
 * Linked lists for files.
 */
static GSList   *fileList = NULL;           // List of files to play
char            *kioskActiveVideo = NULL;   // The active file to play

static pthread_mutex_t statusMutex = PTHREAD_MUTEX_INITIALIZER;

static char readbuf[PATH_MAX];
static char *inbuf;
static int idx=0;

/* Player status information */
static unsigned long long duration=0;        // Video length in microseconds
static unsigned long long position=0;        // Current video position in microseconds
static unsigned long long lastPosition=0;
static int playing=0;
static int stalled=0;
static int nostart=0;

static guint kiosk_status_timer=0;
static guint kiosk_next_timer=0;

/*
 * Video formats we support.  omxplayer supports others
 * but let's just deal with popular formats.
 */
static char *suffices[] = {
    "avi",
    "mp4",
    "mkv",
    "m4v",
    "mov",
    NULL
};

/*
 * Local prototypes
 */
void kioskResetPosition ();

/*
 *========================================================================
 *========================================================================
 * Static Functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getFiles
 * Prototype:  void getFiles( void )
 *
 * Description:
 * Generate a list of video files.
 *========================================================================
 */
static gint 
getFiles ( GSList **list )
{
    int     i;
    FILE    *pd = NULL;
    char    findCmd[128];
    char    *pathPtr;
    char    *filename;
    char    *suffix;

    if ( isCLIFlagSet( CLI_TEST) )
    {
        sprintf(findCmd, "find -L . -type f | sort");
    }
    else
    {
        sprintf(findCmd, "find . -type f | sort");
    }
    piboxLogger(LOG_INFO, "Find command: %s\n", findCmd);

    // Find all files the current directory tree.
    pd = popen(findCmd, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find video files");
        return -1;
    }

    // Read each line
    while( fgets(readbuf, PATH_MAX, pd) != NULL )
    {
        pathPtr = readbuf;
        pathPtr = piboxTrim(pathPtr);
        piboxStripNewline(pathPtr);
        filename = strdup(pathPtr);
        piboxLogger(LOG_INFO, "Filename: %s\n", filename);
        if ( rindex(filename, '.') == NULL )
        {
            free(filename);
            continue;
        }
        suffix = rindex(filename, '.') + 1;
        for(i=0; suffices[i] != NULL; i++)
        {
            if ( strcasecmp(suffix, suffices[i]) == 0 )
                break;
        }
        if ( suffices[i] == NULL )
        {
            free(filename);
            continue;
        }

        piboxLogger(LOG_INFO, "Saveing filename: %s\n", filename);
        *list = g_slist_append(*list, filename);
    }

    pclose(pd);
}

/*
 *========================================================================
 * Name:   kioskGetStatus
 * Prototype:  void kioskGetStatus( gpointer data )
 *
 * Description:
 * Retrieve the status of the currently playing video.
 *
 * Notes:
 * We lock the whole function to avoid getting status of one function while
 * another is being started.  We might not really care, but this implementation
 * means we are getting the current status of the current video.  Always.
 *========================================================================
 */
static gboolean
kioskGetStatus (gpointer data)
{
    static char     *buf;
    char            *name, *value;
    FILE            *pd = NULL;
    char            *token = NULL;

    /* Prevent starting a new video while we get current video status. */
    pthread_mutex_lock( &statusMutex );

    if ( buf == NULL )
    {
        /* Just do this once - we hang onto the buffer for the life of the program. */
        buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_STATUS ) + 2 );
        sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_STATUS);
    }
    piboxLogger(LOG_TRACE3, "Getting player status: %s\n", buf);

    /* Issue the request. */
    pd = popen(buf, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't get player status: %s\n", strerror(errno) );
        pthread_mutex_unlock( &statusMutex );

        /* Keep it going - maybe it's a anomaly. */
        return(TRUE);
    }

    /* Read the output from the request. */
    while( fgets(readbuf, PATH_MAX, pd) != NULL )
    {
        piboxLogger(LOG_TRACE2, "Raw player status line: %s\n", readbuf );

        name = strtok_r(readbuf, ":", &token);
        value = strtok_r(NULL, ":", &token);
        if ( strcasecmp(name, "Duration") == 0 )
        {
            duration=atoll(value);
        }
        else if ( strcasecmp(name, "Position") == 0 )
        {
            position=atoll(value);
        }
        else if ( strcasecmp(name, "Paused") == 0 )
        {
            if ( strcasecmp(value, "false") == 0 )
                playing=1;
            else
                playing=0;
        }
    }

    pclose(pd);

    piboxLogger(LOG_TRACE1, "Playing / Duration / Position: %d / %ll / %ll \n", playing, duration, position );

    /* Release the function that starts the next video. */
    pthread_mutex_unlock( &statusMutex );

    /* keep the timer running */
    return(TRUE);
}

/*
 *========================================================================
 * Name:   kioskStartNextVideo
 * Prototype:  gboolean kioskStartNextVideo( gpointer data )
 *
 * Description:
 * Test if it's time to play the next video.  If so, start it.
 *========================================================================
 */
static gboolean
kioskStartNextVideo (gpointer data)
{
    unsigned long long remainingTime;
    
    /* Pause the timer function while we start a new video. */
    pthread_mutex_lock( &statusMutex );

    if ( (duration <= (unsigned long long)0) || (position<=(unsigned long long)0) )
    {
        piboxLogger(LOG_INFO, "Video hasn't started yet.\n");
        nostart++;
        if ( nostart == STALL_MAX )
        {
            piboxLogger(LOG_ERROR, "Player is stuck - restarting it.\n");
            nostart = 0;
            g_source_remove(kiosk_status_timer);
            g_source_remove(kiosk_next_timer);
            shutdownPlayerProcessor();
            g_timeout_add(1750, startKiosk, NULL);
        }
        pthread_mutex_unlock( &statusMutex );
        return(TRUE);
    }

    /*
     * Check if the player is stalled - re: not playing the video.
     * If we've been stalled for 5s (see STALL_MAX) then we restart the player.
     */
    if ( (unsigned long long)position == (unsigned long long)lastPosition )
    {
        stalled++;
        piboxLogger(LOG_INFO, "Player stalled: %d, pos: %llu, last: %llu\n", stalled, position, lastPosition);
    }
    else
    {
        nostart=0;
        stalled = 0;
        lastPosition = position;
    }

    /* Get the remaining time of the running video, in microseconds */
    remainingTime = (unsigned long long)(duration - position);
    piboxLogger(LOG_TRACE3, "Remaining time: %llu\n", remainingTime);

    /* Test if the remaining time is less than 1.25 seconds. */
    if ( (remainingTime < (unsigned long long)1250000) || (stalled == STALL_MAX))
    {
        if (remainingTime < (unsigned long long)1250000)
            piboxLogger(LOG_INFO, "Remaining time is less than 1.25 seconds: %llu (d: %llu, p: %llu)\n", 
                    remainingTime, duration, position);

        /* Get the next video */
        piboxLogger(LOG_INFO, "Getting next video.\n");
        getNextVideo();

        /* Reset status */
        position=0;
        duration=0;
        playing=0;

        /* If stalled, restart the player. */
        if ( stalled == STALL_MAX )
        {
            piboxLogger(LOG_ERROR, "Player has stalled - restarting it.\n");
            stalled = 0;
            g_source_remove(kiosk_status_timer);
            g_source_remove(kiosk_next_timer);
            shutdownPlayerProcessor();
            g_timeout_add(5000, startKiosk, NULL);
        }
        else
        {
            /* Tell the player to play it. */
            piboxLogger(LOG_INFO, "Starting next video.\n");
            kioskOpenURI( kioskActiveVideo );
            usleep((useconds_t)(remainingTime/(unsigned long long)2));
            kioskResetPosition();
        }
    }

    /* Release the timer to get status updates. */
    pthread_mutex_unlock( &statusMutex );

    /* Keep the timer running */
    return(TRUE);
}

/*
 *========================================================================
 *========================================================================
 * Public Functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   dbLoadKiosk
 * Prototype:  void dbLoadKiosk(Kiosk void )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for files from the current location.
 *========================================================================
 */
void 
dbLoadKiosk()
{
    /*
     * Create list of files to play.
     */
    getFiles(&fileList);
    idx = 0;
    kioskActiveVideo = g_slist_nth_data(fileList, idx);
}

/*
 *========================================================================
 * Name:   getNextVideo
 * Prototype:  void getNextVideo( void )
 *
 * Description:
 * Make the next video the active video.  
 *========================================================================
 */
void
getNextVideo( void )
{
    idx++;
    if ( idx > g_slist_length(fileList)-1 )
    {
        /* Just wrap back to the beginning */
        idx = 0;
    }
    kioskActiveVideo = g_slist_nth_data(fileList, idx);
}

/*
 *========================================================================
 * Name:   getPrevVideo
 * Prototype:  void getPrevVideo( void )
 *
 * Description:
 * Make the previous video the active video.  
 *========================================================================
 */
void
getPrevVideo( void )
{
    idx--;
    if ( idx < 0 )
    {
        /* Just wrap back to the end */
        idx = g_slist_length(fileList) - 1;
    }
    kioskActiveVideo = g_slist_nth_data(fileList, idx);
}

/*
 *========================================================================
 * Name:   kioskStartStatusTimer
 * Prototype:  void kioskStartStatusTimer( void )
 *
 * Description:
 * Start the timer that checks status periodically.
 *========================================================================
 */
void
kioskStartStatusTimer ()
{
    kiosk_status_timer = g_timeout_add(250, kioskGetStatus, NULL);
}

/*
 *========================================================================
 * Name:   kioskStartNextTimer
 * Prototype:  void kioskStartNextTimer( void )
 *
 * Description:
 * Start the timer that checks running the next video.
 *========================================================================
 */
void
kioskStartNextTimer ()
{
    kiosk_next_timer = g_timeout_add(250, kioskStartNextVideo, NULL);
}

/*
 *========================================================================
 * Name:   kioskOpenURI
 * Prototype:  void kioskOpenURI( void )
 *
 * Description:
 * Send message to player to open a new URI (re: start a new video).
 *========================================================================
 */
void
kioskOpenURI ()
{
    char *buf;

    /* Must do this every time because the video path changes each time. */
    buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_OPENURI ) + strlen(kioskActiveVideo)+ 5 );
    sprintf(buf, "%s %s \"%s\"", DBUS_SCRIPT, DBUS_OPENURI, kioskActiveVideo);
    piboxLogger(LOG_INFO, "Sending DBus notification: %s\n", buf);
    system(buf);
    free(buf);
}

/*
 *========================================================================
 * Name:   kioskPause
 * Prototype:  void kioskPause( void )
 *
 * Description:
 * Send message to player to pause current video.
 *========================================================================
 */
void
kioskPause ()
{
    static char *buf = NULL;

    if ( buf == NULL )
    {
        /* Do this once.  We'll hang onto the buffer for the life of the app. */
        buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_PAUSE ) + 2 );
        sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_PAUSE);
    }
    piboxLogger(LOG_INFO, "DBus notification: %s\n", buf);
    system(buf);
}

/*
 *========================================================================
 * Name:   kioskPlay
 * Prototype:  void kioskPlay( void )
 *
 * Description:
 * Send message to player to pause current video.
 *========================================================================
 */
void
kioskPlay ()
{
    static char *buf = NULL;

    if ( buf == NULL )
    {
        /* Do this once.  We'll hang onto the buffer for the life of the app. */
        buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_PLAY ) + 2 );
        sprintf(buf, "%s %s", DBUS_SCRIPT, DBUS_PLAY);
    }
    piboxLogger(LOG_INFO, "DBus notification: %s\n", buf);
    system(buf);
}

/*
 *========================================================================
 * Name:   kioskResetPosition
 * Prototype:  void kioskResetPosition( void )
 *
 * Description:
 * Reset the playback position, in the hopes it will reset video metadata.
 *========================================================================
 */
void
kioskResetPosition ()
{
    static char *buf = NULL;

    if ( buf == NULL )
    {
        /* Do this once.  We'll hang onto the buffer for the life of the app. */
        buf = calloc( 1, strlen(DBUS_SCRIPT) + strlen( DBUS_SETPOSITION ) + 4 );
        sprintf(buf, "%s %s 0", DBUS_SCRIPT, DBUS_SETPOSITION);
    }
    piboxLogger(LOG_INFO, "Sending DBus notification: %s\n", buf);
    system(buf);
}

/*
 *========================================================================
 * Name:   kioskTimeLeft
 * Prototype:  int kioskTimeLeft( void )
 *
 * Description:
 * Returns the number of microseconds left in the video.
 *========================================================================
 */
int
kioskTimeLeft ()
{
    unsigned long long remainingTime;

    pthread_mutex_lock( &statusMutex );
    remainingTime = duration - position;
    pthread_mutex_unlock( &statusMutex );
    return( remainingTime );
}

