/*******************************************************************************
 * videofe
 *
 * db.c:  Functions for reading a VideoLib database.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <sys/inotify.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "videofe.h"

#define MOVIE_TYPE      1
#define EPISODE_TYPE    2

/*
 * Movie JSON fields
 */
#define VACTIVE     "videolib_active"
#define VFILENAME   "videolib_filename"
#define VTITLE      "videolib_title"
#define VVIDEOPATH  "videolib_path"
#define VMIMAGE     "videolib_movie_image"

/* Common field */
#define VOVERVIEW   "videolib_overview"

/*
 * TV Episodes JSON fields
 */
#define VSERIESID   "videolib_active_series"
#define VEPISODEID  "videolib_active_episode"
#define VFILENAME   "videolib_filename"
#define VETITLE     "videolib_episode_title"
#define VRTITLE     "videolib_real_title"
#define VTOKEN      "videolib_token"
#define VTOKENTS    "videolib_token_ts"
#define VVIDEOPATH  "videolib_path"
#define VEIMAGE     "videolib_episode_image"
#define VSIMAGE     "videolib_series_image"

/*
 * Linked lists for db files.
 */
GSList  *movieDirList = NULL;       // List of directories for movies
GSList  *episodeDirList = NULL;     // List of directories for TV episodes
GSList  *movieList = NULL;          // List of movies
GSList  *episodeList = NULL;        // List of TV episodes

GSList  *activeVideo = NULL;

static char readbuf[PATH_MAX];
static char *inbuf;

static int inotify_fd = -1;         // inotify file descriptor

static pthread_mutex_t dbMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * -----------------------------------------------------------------------
 * Name:   watchDirs
 * Prototype:  int watchDirs( void )
 *
 * Description:
 * Setup to watch directories under DBTOP for changes that will cause us to update the db.
 *
 * Returns:
 * inotify_fd on success (inotify is enabled).
 * -1 on failure (inotify is not enabled).
 * -----------------------------------------------------------------------
 */
int 
watchDirs ( void )
{
    int     wd;
    char    *watch_dir;
    char    *buf;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Open an inotify instance */
    inotify_fd = inotify_init();
    if (inotify_fd < 0)
    {
        piboxLogger(LOG_ERROR, "inotify_init(): %s\n", strerror(errno));
        return -1;
    }

    /*
     * Watch for the creation and deletion of directories under DBTOP.
     * Directories watched under DBTOP:
     *    usb
     *    smb
     */
    piboxLogger(LOG_INFO, "Attempting to watch for directories under: %s\n", cliOptions.dbTop);
    buf = (char *)calloc(1, strlen(cliOptions.dbTop) + 3 + 2);
    sprintf(buf, "%s/usb", cliOptions.dbTop);
    wd = inotify_add_watch (inotify_fd, buf, IN_CREATE | IN_DELETE);
    if (wd < 0)
    {
        piboxLogger(LOG_ERROR, "Cannot add watch for \"%s\" with event mask %lX", buf, IN_CREATE | IN_DELETE);
        close(inotify_fd);
        inotify_fd = -1;
        free(buf);
        return -1;
    }
    sprintf(buf, "%s/smb", cliOptions.dbTop);
    wd = inotify_add_watch (inotify_fd, buf, IN_CREATE | IN_DELETE);
    if (wd < 0)
    {
        piboxLogger(LOG_ERROR, "Cannot add watch for \"%s\" with event mask %lX", buf, IN_CREATE | IN_DELETE);
        close(inotify_fd);
        inotify_fd = -1;
        free(buf);
        return -1;
    }
    free(buf);

    piboxLogger(LOG_INFO, "Setup watch directories.\n");
    return inotify_fd;
}

/*
 * -----------------------------------------------------------------------
 * Name:   stopWatchDirs
 * Prototype:  void stopWatchDirs( void )
 *
 * Description:
 * Stop watching directories for changes.
 * -----------------------------------------------------------------------
 */
void 
stopWatchDirs ( void )
{
    /* Open an inotify instance */
    if ( inotify_fd != -1 )
    {
        close(inotify_fd);
        inotify_fd = -1;
    }
}

/*
 * -----------------------------------------------------------------------
 * Name:   getWatchFD
 * Prototype:  int getWatchFD( void )
 *
 * Description:
 * Provide the inotify file descriptor to the caller.
 *
 * Returns:
 * The file descriptor currently being used for inotify events.
 * -----------------------------------------------------------------------
 */
int 
getWatchFD ( void )
{
    return inotify_fd;
}

/*
 * -----------------------------------------------------------------------
 * Name:   freeDirList
 * Prototype:  int freeDirList( void )
 *
 * Description:
 * Frees memory allocated for each list item.
 * -----------------------------------------------------------------------
 */
void
freeDirList( gpointer item, gpointer user_data )
{
    if ( item != NULL )
        g_free(item);
}

/*
 * -----------------------------------------------------------------------
 * Name:   freeList
 * Prototype:  int freeList( void )
 *
 * Description:
 * Frees memory allocated for each Movie or TV episode list item.
 * -----------------------------------------------------------------------
 */
void
freeList( gpointer item, gpointer user_data )
{
    if ( item != NULL )
    {
        if ( GPOINTER_TO_UINT(user_data) == MOVIE_TYPE )
        {
            g_free(((MOVIE_T *)item)->basedir);
            g_free((MOVIE_T *)item);
        }
        else
        {
            g_free(((EPISODE_T *)item)->basedir);
            g_free((EPISODE_T *)item);
        }
    }
}

/*
 * -----------------------------------------------------------------------
 * Name:   getDirs
 * Prototype:  gint getDirs( char *type, GSList **list )
 *
 * Description:
 * Generate a list of db directories.
 * -----------------------------------------------------------------------
 */
gint 
getDirs ( char *type, GSList **list )
{
    FILE    *pd = NULL;
    char    findCmd[128];
    char    *pathPtr;
    char    *dir;

    if ( isCLIFlagSet( CLI_TEST) )
    {
        sprintf(findCmd, "find -L . -type d -name %s", type);
    }
    else
    {
        sprintf(findCmd, "find . -type d -name %s", type);
    }
    piboxLogger(LOG_INFO, "Find command: %s\n", findCmd);

    // Find all databases in top level path.
    pd = popen(findCmd, "r");
    if ( pd == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find database directories");
        return -1;
    }

    /* Clear the current list, if any */
    piboxLogger(LOG_INFO, "Clearing directory list.\n");
    g_slist_foreach(*list, freeDirList, NULL);
    g_slist_free(*list);
    *list = NULL;

    /* Read each line */
    piboxLogger(LOG_INFO, "Building directory list.\n");
    while( fgets(readbuf, PATH_MAX, pd) != NULL )
    {
        pathPtr = readbuf;
        pathPtr = piboxTrim(pathPtr);
        piboxStripNewline(pathPtr);
        dir = strdup(pathPtr);
        piboxLogger(LOG_INFO, "Video dir: %s\n", dir);
        *list = g_slist_append(*list, dir);
    }

    piboxLogger(LOG_INFO, "Done finding directories to scan.\n");
    pclose(pd);
}

/*
 * -----------------------------------------------------------------------
 * Name:   readDB
 * Prototype:  void readDB( void )
 *
 * Description:
 * Read JSON data from the specified directory.
 * -----------------------------------------------------------------------
 */
void 
readDB ( gconstpointer item, gconstpointer user_data )
{
    DIR             *dir;
    struct dirent   *ent;
    char            *path = (char *)item;
    char            *basedir;
    char            *idx;
    int             type = GPOINTER_TO_INT(user_data);
    MOVIE_T         *movie;
    EPISODE_T       *episode;
    char            filename[PATH_MAX];
    FILE            *fd;
    int             i;
    struct stat     stat_buf;

    /* We might get this while watching the DB tree for new events. */
    if ( path == NULL )
    {
        piboxLogger(LOG_ERROR, "No directory provided. Skipping.\n");
        return;
    }

    piboxLogger(LOG_INFO, "Directory to scan: %s\n", path);
    basedir = strdup(path);
    idx = strstr(basedir, "/videolib");
    if ( idx == NULL )
    {
        piboxLogger(LOG_INFO, "Missing /videolib - skipping directory.\n");
        return;
    }
    *idx = '\0';

    if ( (dir = opendir (path)) != NULL) 
    {
        while ((ent = readdir (dir)) != NULL) 
        {
            if ( strncmp(ent->d_name, ".", 1) == 0 )
                continue;

            if ( type == MOVIE_TYPE )
            {
                /* Read the file into a movie struct. */
                sprintf(filename, "%s/%s", path, ent->d_name);
                piboxLogger(LOG_INFO, "movie json file: %s\n", filename);

                if ( stat(filename, &stat_buf) != 0 )
                {
                    piboxLogger(LOG_ERROR, "Error stat of file: %s, reason = %d\n", filename, errno);
                    continue;
                }
                if ( stat_buf.st_size > 0 )
                {
                    fd = fopen(filename, "r");
                    if ( fd != NULL )
                    {
                        movie = g_malloc0(sizeof(MOVIE_T));
                        inbuf = g_malloc0(stat_buf.st_size+1);
                        fgets(inbuf, stat_buf.st_size, fd);
                        movie->json = inbuf;
                        movie->basedir = strdup(basedir);
                        piboxLogger(LOG_TRACE2, "Adding to movieList: %s\n", inbuf);
                        movieList = g_slist_append(movieList, movie);
                        fclose(fd);
                    }
                    else
                    {
                        piboxLogger(LOG_ERROR, "Failed fopen on %s: reason = %s\n", filename, strerror(errno));
                    }
                }
                else
                {
                    piboxLogger(LOG_ERROR, "Failed fopen on %s: st_size = 0\n", filename);
                }
            }
            else
            {
                sprintf(filename, "%s/%s", path, ent->d_name);
                piboxLogger(LOG_INFO, "tv json file: %s\n", filename);

                if ( stat(filename, &stat_buf) != 0 )
                {
                    piboxLogger(LOG_ERROR, "Error stat of file: %s, reason = %d\n", filename, errno);
                    continue;
                }
                piboxLogger(LOG_INFO, "File exists.\n");

                if ( stat_buf.st_size > 0 )
                {
                    fd = fopen(filename, "r");
                    if ( fd != NULL )
                    {
                        piboxLogger(LOG_INFO, "Reading episode.\n");
                        episode = g_malloc0(sizeof(EPISODE_T));
                        inbuf = g_malloc0(stat_buf.st_size+1);
                        fgets(inbuf, stat_buf.st_size, fd);
                        episode->series_json = inbuf;
                        episode->basedir = strdup(basedir);
                        piboxLogger(LOG_TRACE2, "Adding to episodeList: %s\n", inbuf);
                        episodeList = g_slist_append(episodeList, episode);
                        fclose(fd);
                    }
                    else
                    {
                        piboxLogger(LOG_ERROR, "Failed fopen on %s: reason = %s\n", filename, strerror(errno));
                    }
                }
                else
                {
                    piboxLogger(LOG_ERROR, "Failed stat on %s: st_size = 0\n", filename);
                }
            }
        }
        closedir (dir);
    } 
    else
    {
        /* could not open directory */
        piboxLogger(LOG_INFO, "Failed to open movie diretory: %s\n", path);
    }

    free(basedir);
}

/*
 * -----------------------------------------------------------------------
 * Name:   genVideo
 * Prototype:  void genVideo( void )
 *
 * Description:
 * Extract required fields from each video type.
 * -----------------------------------------------------------------------
 */
gint 
genVideo ( gconstpointer item, gconstpointer user_data )
{
    MOVIE_T         *movie;
    EPISODE_T       *episode;
    int             type = GPOINTER_TO_INT(user_data);
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object
    char            *value;
    char            *posterPath;
    char            *posters = "/posters";
    int             length;
    int             rc;
    struct stat     stat_buf;

    piboxLogger(LOG_TRACE1, "Entered.\n");
    if ( type == MOVIE_TYPE )
    {
        piboxLogger(LOG_INFO, "type == Movie\n");
        movie = (MOVIE_T *)item;

        /* Assume the entry is valid.  We'll check later to be certain. */
        movie->valid = DB_VALID;

        piboxLogger(LOG_INFO, "Basedir: %s\n", movie->basedir);
        piboxLogger(LOG_INFO, "JSON: %s\n", movie->json);
        root_value = json_parse_string(movie->json);
        root_object = json_value_get_object(root_value);

        value = (char *)json_object_get_string( root_object, VTITLE );
        if ( value != NULL ) 
        {
            piboxLogger(LOG_INFO, "VTITLE: %s\n", value);
            movie->title = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VTITLE.\n");

        value = (char *)json_object_get_string( root_object, VVIDEOPATH );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VVIDEOPATH: %s\n", value);
            movie->path = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VVIDEOPATH.\n");

        value = (char *)json_object_get_string( root_object, VMIMAGE );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VMIMAGE: %s\n", value);
            movie->poster = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VMIMAGE.\n");

        value = (char *)json_object_get_string( root_object, VOVERVIEW );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VOVERVIEW: %s\n", value);
            movie->overview = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VOVERVIEW.\n");

        if ( (movie->title == NULL) ||
             (movie->path == NULL) ||
             (movie->overview == NULL) )
        {
            piboxLogger(LOG_INFO, "DB entry is invalid (missing some fields).\n");
            movie->valid = DB_INVALID;
        }
        else
        {
            /* Make sure poster exists */
            if ( movie->poster != NULL )
            {
                if ( *movie->poster != '/')
                    posters = "/posters/";
                length = strlen(movie->basedir) + strlen(posters) + strlen(movie->poster) + 1;
                posterPath = g_malloc0(length);
                sprintf(posterPath, "%s%s%s", movie->basedir, posters, movie->poster);
                rc = stat(posterPath, &stat_buf);
                if ( rc != 0 )
                {
                    piboxLogger(LOG_ERROR, "Can't find movie poster: %s\n", posterPath);
                    movie->valid = DB_INVALID;
                }
                free(posterPath);
            }
            else
            {
                piboxLogger(LOG_ERROR, "Missing movie poster.\n");
                movie->valid = DB_INVALID;
            }
        }

        /* Free up json - we don't need it anymore */
        json_value_free(root_value);
        free(movie->json);
    }
    else
    {
        piboxLogger(LOG_INFO, "type == Episode\n");
        episode = (EPISODE_T *)item;

        /* Assume the entry is valid.  We'll check later to be certain. */
        episode->valid = DB_VALID;

        piboxLogger(LOG_INFO, "Basedir: %s\n", episode->basedir);
        root_value = json_parse_string(episode->series_json);
        root_object = json_value_get_object(root_value);

        value = (char *)json_object_get_string( root_object, VSERIESID );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VSERIESID: %s\n", value);
            episode->episode_id = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VSERIESID.\n");

        value = (char *)json_object_get_string( root_object, VEPISODEID );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VEPISODEID: %s\n", value);
            episode->series_id = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VEPISODEID.\n");

        value = (char *)json_object_get_string( root_object, VETITLE );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VETITLE: %s\n", value);
            episode->episode_title = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VETITLE.\n");

        value = (char *)json_object_get_string( root_object, VRTITLE );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VRTITLE: %s\n", value);
            episode->real_title = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VRTITLE.\n");

        value = (char *)json_object_get_string( root_object, VVIDEOPATH );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VVIDEOPATH: %s\n", value);
            episode->path = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VVIDEOPATH.\n");

        value = (char *)json_object_get_string( root_object, VEIMAGE );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VEIMAGE: %s\n", value);
            episode->eposter = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VEIMAGE.\n");

        value = (char *)json_object_get_string( root_object, VSIMAGE );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VSIMAGE: %s\n", value);
            episode->sposter = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VSIMAGE.\n");

        value = (char *)json_object_get_string( root_object, VOVERVIEW );
        if ( value != NULL )
        {
            piboxLogger(LOG_INFO, "VOVERVIEW: %s\n", value);
            episode->overview = strdup(value);
        }
        else
            piboxLogger(LOG_ERROR, "Missing VOVERVIEW.\n");

        if ( (episode->episode_id == NULL) ||
             (episode->series_id == NULL) ||
             (episode->episode_title == NULL) ||
             (episode->real_title == NULL) ||
             (episode->path == NULL) ||
             (episode->overview == NULL) )
        {
            piboxLogger(LOG_INFO, "DB entry is invalid (missing some fields).\n");
            episode->valid = DB_INVALID;
        }
        else
        {
            /* Make sure posters exists */
            if ( (episode->eposter != NULL) && (episode->sposter != NULL) )
            {
                if ( *episode->eposter != '/')
                    posters = "/posters/";
                length = strlen(episode->basedir) + strlen(posters) + strlen(episode->eposter) + 1;
                posterPath = g_malloc0(length);
                sprintf(posterPath, "%s%s%s", episode->basedir, posters, episode->eposter);
                rc = stat(posterPath, &stat_buf);
                if ( rc != 0 )
                {
                    piboxLogger(LOG_ERROR, "Can't find episode poster: %s\n", posterPath);
                    episode->valid = DB_INVALID;
                }
                free(posterPath);

                if ( episode->valid == DB_VALID )
                {
                    posters = "/posters";
                    if ( *episode->sposter != '/')
                        posters = "/posters/";
                    length = strlen(episode->basedir) + strlen(posters) + strlen(episode->sposter) + 1;
                    posterPath = g_malloc0(length);
                    sprintf(posterPath, "%s%s%s", episode->basedir, posters, episode->sposter);
                    rc = stat(posterPath, &stat_buf);
                    if ( rc != 0 )
                    {
                        piboxLogger(LOG_ERROR, "Can't find series poster: %s\n", posterPath);
                        episode->valid = DB_INVALID;
                    }
                    free(posterPath);
                }
            }
            else
            {
                piboxLogger(LOG_ERROR, "Missing either episode or series poster (or both).\n");
                episode->valid = DB_INVALID;
            }
        }

        /* Free up json - we don't need it anymore */
        json_value_free(root_value);
        free(episode->series_json);
    }
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( void )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for db's from the current location.
 *========================================================================
 */
void 
dbLoad()
{
    char    *path;
    char    *displayName;
    char    *tmdbID;
    char    *poster;
    char    *releaseDate;
    char    *tsave = NULL;

    GSList  *item  = NULL;

    char    *findCmd = NULL;
    FILE    *pd = NULL;
    FILE    *vd = NULL;
    char    buf[MAXBUF];
    char    videobuf[MAXBUF];
    char    dbPath[MAXBUF];
    char    *pathPtr, *linePtr, *ptr;
    int     firstLine = 0;
    int     cnt = 0;
    char    *dup, *suffix, *cmd;

    /*
     * Create list of directories for each type of db.
     */
    piboxLogger(LOG_INFO, "Looking for movie directories to scan.\n");
    getDirs("movies", &movieDirList);
    piboxLogger(LOG_INFO, "Looking for TV directories to scan.\n");
    getDirs("tv", &episodeDirList);

    pthread_mutex_lock( &dbMutex );

    /* Clear the current lists, if any */
    activeVideo=NULL;
    if ( movieList != NULL )
    {
        g_slist_foreach(movieList, freeList, GINT_TO_POINTER(MOVIE_TYPE));
        g_slist_free(movieList);
        movieList = NULL;
    }
    if ( episodeList != NULL )
    {
        g_slist_foreach(episodeList, freeList, GINT_TO_POINTER(EPISODE_TYPE));
        g_slist_free(episodeList);
        episodeList = NULL;
    }

    /* 
     * Run the lists and read in the json files in each directory.
     */
    piboxLogger(LOG_INFO, "Calling readDB on movieDirList\n");
    g_slist_foreach(movieDirList, (GFunc)readDB, GINT_TO_POINTER(MOVIE_TYPE));
    piboxLogger(LOG_INFO, "Calling readDB on episodeDirList\n");
    g_slist_foreach(episodeDirList, (GFunc)readDB, GINT_TO_POINTER(EPISODE_TYPE));

    /*
     * Run through each list of video types and generate the required
     * data.
     */
    piboxLogger(LOG_INFO, "Calling genVideo on movieList\n");
    g_slist_foreach(movieList, (GFunc)genVideo, GINT_TO_POINTER(MOVIE_TYPE));
    piboxLogger(LOG_INFO, "Calling genVideo on episodeList\n");
    g_slist_foreach(episodeList, (GFunc)genVideo, GINT_TO_POINTER(EPISODE_TYPE));

    pthread_mutex_unlock( &dbMutex );
}

/*
 *========================================================================
 * Name:   addToDisplay
 * Prototype:  void addToDisplay( gpointer, gpointer )
 *
 * Description:
 * Add an entry from the db list to the display.
 *========================================================================
 */
void 
addToDisplay (gpointer item, gpointer user_data)
{
    GtkListStore    *list  = (GtkListStore *)user_data;
    GtkTreeIter     iter;
    MOVIE_T         *movie;
    EPISODE_T       *episode;
    char            *title;
    char            *token;
    char            buf[128];

    if ( getChoice() == DB_MOVIE )
    {
        movie = (MOVIE_T *)item;
        if ( movie->valid == DB_VALID )
        {
            // Acquire an iterator
            gtk_list_store_append (list, &iter);

            // Save the data in the model
            gtk_list_store_set(list, &iter, 0, movie->title, -1);
        }
    }
    else
    {
        episode = (EPISODE_T *)item;
        if ( episode->valid == DB_VALID )
        {
            // Acquire an iterator
            gtk_list_store_append (list, &iter);

            // Save the data in the model
            title = strdup(episode->episode_title);
            token = strtok(title, "-");
            memset(buf, 0, 128);
            sprintf(buf, "%s- %s", token, episode->real_title);
            gtk_list_store_set(list, &iter, 0, buf, -1);
            free(title);
        }
    }
}

/*
 *========================================================================
 * Name:   populateList
 * Prototype:  void populateList( GtkWidget * )
 *
 * Description:
 * Fills a list widget with the names of the movies.
 *========================================================================
 */
void
populateList (GtkListStore *listStore)
{
    piboxLogger(LOG_TRACE1, "Entered populateList, choice = %d\n", getChoice());
    pthread_mutex_lock( &dbMutex );
    if ( getChoice() == DB_MOVIE )
    {
        piboxLogger(LOG_TRACE1, "Populating to movies.\n");
        gtk_list_store_clear(listStore);
        g_slist_foreach(movieList, addToDisplay, listStore);
    }
    else
    {
        piboxLogger(LOG_TRACE1, "Populating to episodes.\n");
        gtk_list_store_clear(listStore);
        g_slist_foreach(episodeList, addToDisplay, listStore);
    }
    pthread_mutex_unlock( &dbMutex );
}

/*
 *========================================================================
 * Name:   findVideoByName
 * Prototype:  gint findVideoByName( gconstpointer, gconstpointer, )
 *
 * Description:
 * Find the video entry based on the display name.
 *========================================================================
 */
gint
findVideoByName( gconstpointer item, gconstpointer user_data )
{
    MOVIE_T     *movie;
    EPISODE_T   *episode;
    char        *name = (char *)user_data;
    char        *title;
    char        *token;
    char        buf[128];

    if ( name == NULL )
    {   
        fprintf(stderr, "missing video name to search for\n");
        return 0;
    }

    if ( getChoice() == DB_MOVIE )
    {
        movie = (MOVIE_T *)item;
        if ( strcmp(movie->title, name) == 0 )
            return 0;
    }
    else
    {
        episode = (EPISODE_T *)item;
        title = strdup(episode->episode_title);
        token = strtok(title, "-");
        memset(buf, 0, 128);
        sprintf(buf, "%s- %s", token, episode->real_title);
        if ( strcmp(buf, name) == 0 )
            return 0;
    }
    return 1;
}

/*
 *========================================================================
 * Name:   updatePoster
 * Prototype:  void updatePoster( GtkTreeSelection *, gpointer )
 *
 * Description:
 * Update the posted displayed
 *========================================================================
 */
void
updatePoster (GtkTreeSelection *selection, gpointer data)
{
    GtkTreeIter     iter;
    GtkTreeModel    *model;
    gchar           *videoName;
    GtkRequisition  req;
    int             length;
    MOVIE_T         *movie;
    EPISODE_T       *episode;
    char            *posters = "/posters";

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Clear old paths */
    if ( imagePath != NULL )
    {
        g_free(imagePath);
        imagePath = NULL;
    }
    if ( seriesImagePath != NULL )
    {
        g_free(seriesImagePath);
        seriesImagePath = NULL;
    }

    if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
        /* Extract selected video from displayed list of video names */
        gtk_tree_model_get (model, &iter, 0, &videoName, -1);
        
        /* Find that video in our database */
        if ( getChoice() == DB_MOVIE )
            activeVideo = g_slist_find_custom(movieList, videoName, (GCompareFunc)findVideoByName);
        else
            activeVideo = g_slist_find_custom(episodeList, videoName, (GCompareFunc)findVideoByName);
        g_free (videoName);
        if ( activeVideo == NULL )
        {
            piboxLogger(LOG_ERROR, "No active video. Not updating poster.\n");
            goto updatePosterExit;
        }

        /* Build path to video image */
        if ( getChoice() == DB_MOVIE )
        {
            movie = (MOVIE_T *)activeVideo->data;
            if ( *movie->poster != '/')
                posters = "/posters/";
            length = strlen(movie->basedir) + strlen(posters) + strlen(movie->poster) + 1;
            imagePath = g_malloc0(length);
            sprintf(imagePath, "%s%s%s", movie->basedir, posters, movie->poster);

            /* Save current overview */
            overview = movie->overview;
            piboxLogger(LOG_INFO, "Movie poster path: %s\n", imagePath);
        }
        else
        {
            episode = (EPISODE_T *)activeVideo->data;
            if ( *episode->eposter != '/' )
                posters = "/posters/";

            /* Episode image */
            length = strlen(episode->basedir) + strlen(posters) + strlen(episode->eposter) + 1;
            imagePath = g_malloc0(length);
            sprintf(imagePath, "%s%s%s", episode->basedir, posters, episode->eposter);
            piboxLogger(LOG_INFO, "Episode poster path: %s\n", imagePath);

            length = strlen(episode->basedir) + strlen(posters) + strlen(episode->sposter) + 1;
            seriesImagePath = g_malloc0(length);
            sprintf(seriesImagePath, "%s%s%s", episode->basedir, posters, episode->sposter);
            piboxLogger(LOG_INFO, "Series poster path: %s\n", seriesImagePath);

            /* Save current overview */
            overview = episode->overview;
        }

    }
    else
    {
        piboxLogger(LOG_INFO, "No selection available. Poster not updated.\n");
    }

updatePosterExit:
    do_drawing();
    return;
}

/*
 *========================================================================
 * Name:   playVideo
 * Prototype:  int playVideo( void )
 *
 * Description:
 * Play the currently active video.
 *
 * Returns
 * TRUE if the video is being played.
 * FALSE if the video can't be displayed.
 *========================================================================
 */
int
playVideo()
{
    MOVIE_T         *movie;
    EPISODE_T       *episode;
    char            *path;

    /* It's possible the user pulled the video before we got here. */
    if ( activeVideo == NULL )
        return FALSE;

    pthread_mutex_lock( &dbMutex );
    if ( getChoice() == DB_MOVIE )
    {
        movie = (MOVIE_T *)activeVideo->data;
        path = g_malloc0(strlen(movie->basedir) + strlen(movie->path) + 2);
        sprintf(path, "%s/%s", movie->basedir, movie->path);
    }
    else
    {
        episode = (EPISODE_T *)activeVideo->data;
        path = g_malloc0(strlen(episode->basedir) + strlen(episode->path) + 2);
        sprintf(path, "%s/%s", episode->basedir, episode->path);
    }
    pthread_mutex_unlock( &dbMutex );

    piboxLogger(LOG_INFO, "Video path: %s\n", path);

    // Cleanup from any previous runs
    shutdownPlayerProcessor();

    // Start new video
    startPlayerProcessor(path);

    return TRUE;
}

