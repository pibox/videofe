#!/bin/bash -p
# ------------------------------------------------------

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-r] "
    echo "where"
    echo "-r    Enable show-reachable"
    echo "-v    "
    echo ""
    echo "Uses Valgrind to run the piboxd binary in test mode and check for memory leaks."
    echo "Should be run first, followed by unit tests."
    echo "Use Ctrl-C to stop the test and get leak reports from Valgrind."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
REACHABLE=
VERBOSE=
while getopts ":rv" Option
do
    case $Option in
    r)  REACHABLE="--show-reachable=yes";;
    v)  VERBOSE="-v";;
    *)  doHelp; exit 0;;
    esac
done

valgrind --tool=memcheck --leak-check=full $VERBOSE $REACHABLE --leak-resolution=high \
    --show-leak-kinds=all --num-callers=20 --suppressions=tests/gtk2.suppression \
	src/videofe -T -v3


