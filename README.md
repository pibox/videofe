## Synopsis

VideoFE is a program for browsing and playing videos under a PiBox-based system such as the PiBox Media System.  The program scans the /media/ tree for videos in supported formats and creates a database from which videos can be played.

VideoFE has multiple modes.  By default a list of videos (Movies or TV episodes) will be listed allowing the user to
select a video to play.  In this mode the video will play till the end or till the user stops it, at which time the list
of videos is redisplayed.

In kiosk mode VideoFE will play the complete list of videos, in order, until the user stops the playback.  If the end of
the playlist is reached the playback cycles back to the first video.

VideoFE uses the Raspberry Pi omxplayer for video playback.  VideoFE communicates with omxplayer using the dbus API to allow playlists, which omxplayer does not support natively (it only plays one video at a time).

VideoFE supports the official Raspberry Pi touchscreen display.  Touch actions are defined in the videofe.c:imageTouch() function header and include stop, pause, fast forward, rewind, next video and previous video.

VideoFE is based on GTK and Cairo.

## Build

VideoFE can be built on a local Linux box using autoconf.  A cross compiled version can be built using the cross.sh wrapper script.

### Local Build

To build locally for testing, use the following commands.

    autoreconf -i
    ./configure
    make
    src/videofe -T -v3

The last command will run videofe in test mode and use verbosity level three for debugging.  This mode will only work to display video lists.   Playback will not work on anything other than the Raspberry Pi since omxplayer only runs on that hardware.

For more information on command line options use the following command.

    src/videofe -?

### Cross compile and packaging

To cross compile the application use the cross.sh script.  To get a simple usage statement for this script use the following command.

    ./cross.sh -?

Use of the cross compile script requires specifying three components.

1. The path to the cross compiler toolchain directory.
1. The path to the PiBox root file system staging directory.
1. The path to the opkg tools on the host system.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

Additional information for building can be found in the INSTALL file.

## Installation

VideoFE is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/videofe_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/videofe_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

